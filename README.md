Please-lib
==========

A signle OCaml file library to use while writing configure-like and repository
maintenance scripts (generate `jbuild`, `opam`, etc. files).

**WORK IN PROGRESS**

How To Use
----------

Copy `please_lib.ml` somewhere in your repo (e.g. `tools/`), and then `#use` or
`#mod_use` it.

Examples
--------

Some repos that use it:

- <https://gitlab.com/smondet/genspio-web-demo>

